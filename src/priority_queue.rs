use std::collections::HashMap;

use serde_json::from_slice;
pub trait PriorityQueue<Element> {
    /// create a new priority queue.
    fn new() -> Self;
    /// check whether the queue has no elements.
    fn is_empty(&self) -> bool;
    /// returns the highest-priority element but does not modify the queue.
    fn peek(&self) -> Option<Element>;
    /// add an element to the queue with an associated priority.
    fn insert(&mut self, element: Element, priority: u64);
    /// remove the element from the queue that has the highest priority, and return it.
    fn pop(&mut self) -> Option<Element>;
}

pub struct PriorityQueueImpl(HashMap<Vec<u8>, Vec<u8>>);


// Do not modify anything above ^^^


/// The Type Parameter 'Element'  is  'Vec<u8>'; That is the value-element in the HashMap
/// The Key-element in the Hashmap is the  PriorityQueue's priority; a u64
/// So Ideally the PriorityQueueImpl would have owned a HashMap<u64, Vec<u8>> to hold the priority queue;
/// a conversion from u64 to Vec<u8> is required.
/// If an element is inserted with a priority that already exists, this new element will be merged with the existing element:
/// If for a given priority, string-representation of the stored Vec<u8> is "Bibbidi" and value "Babbidi"
/// is inserted for this priority as well, the resulting stored value becomes ["Bibbidi", "Babbidi"]
impl PriorityQueue<Vec<u8>> for PriorityQueueImpl {
    fn new() -> Self {
        PriorityQueueImpl(HashMap::new())
    }

    fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    fn peek(&self) -> Option<Vec<u8>> {
        match self.0.get::<Vec<u8>>(self.highest_priority_key().as_ref()).or(None).cloned() {
            None => None,
            Some(e) => Some(to_elements(e).get(0).unwrap().clone())
        }
    }

    fn insert(&mut self, element: Vec<u8>, priority: u64) {
        let key = to_vec(priority);
        match self.0.get(&key).cloned() {
            None => self.0.insert(key, element),
            Some(v) => self.0.insert(key, serde_json::ser::to_vec(&merge(v, element)).unwrap())
        };
    }

    fn pop(&mut self) -> Option<Vec<u8>> {
        let key: Vec<u8> = self.highest_priority_key();
        match self.0.get::<Vec<u8>>(key.as_ref()).cloned() {
            None => None,
            Some(e) => {
                //Get the content as a List of String...
                let mut items = to_elements(e);

                if items.len() > 1 {
                    //Return and remove the first one and store the remainder...
                    let first = items.remove(0);
                    self.0.insert(key, serde_json::ser::to_vec(&items).unwrap());
                    Some(first)
                } else {
                    //Return the only item in the list. Remove the entry altogether...
                    self.0.remove::<Vec<u8>>(key.as_ref());
                    Some(items.get(0).unwrap().clone())
                }
            }
        }
    }
}

///Returns the map-key with the highest value
impl PriorityQueueImpl {
    ///Returns the key with the highest value
    fn highest_priority_key(&self) -> Vec<u8> {
        self.0.keys()
            .fold(Vec::<u8>::new(),
                  |best, current| { if *current > best { current.clone() } else { best } },
            )
    }
}

///Extracts the String-value from the data provided.
pub(crate) fn as_string(v: Option<Vec<u8>>) -> String {
    String::from_utf8(v.unwrap()).unwrap_or(String::default())
}

///This function will parse the already stored data into Vec<String>. The new element will be added to the Vec<String>
/// The resulting Vector will be returned
fn merge(original_data: Vec<u8>, element: Vec<u8>) -> Vec<Vec<u8>> {
    let mut a = to_elements(original_data);
    a.push(element);
    a
}

///Parses the stored Vec<u8> into list of stored elements as a Vec<String>
/// If that fails; e.g. because only one element is stored - not in a list - that stored element is returned as a list
fn to_elements(original_data: Vec<u8>) -> Vec<Vec<u8>> {
    from_slice::<Vec<Vec<u8>>>(&original_data.clone()).unwrap_or(vec!(original_data))
}

fn to_vec(priority: u64) -> Vec<u8> {
    priority.to_be_bytes().into()
}
