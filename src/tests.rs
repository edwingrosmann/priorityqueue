use std::time::SystemTime;

use crate::priority_queue::{as_string, PriorityQueue, PriorityQueueImpl};

#[test]
fn stress_test_10_000_items() {
    stress_test(1, 10000);
    stress_test(10, 1000);
    stress_test(100, 100);
    stress_test(1000, 10);
    stress_test(10000, 1);
}


#[test]
fn stress_test_1_prio_50000_items() {
    stress_test(1, 50000);
}

#[test]
fn stress_test_5000_prio_100_items() {
    stress_test(5000, 100);
}

fn stress_test(number_of_prios: u64, number_of_items_per_prio: u64) {
    let mut queue = PriorityQueueImpl::new();
    let mut total_pops = 0;

    let t = SystemTime::now();

    assert!(queue.is_empty());
    for ii in 1..=number_of_items_per_prio {
        for e in 1..=number_of_prios {
            let i = (e % (256 - 3)) as u8;
            queue.insert(vec![i, i + 1, i + 2], ii);
        }
    }

    println!("Created: {} priority items ({} items in {} priorities) in {:?}",
             number_of_items_per_prio * number_of_prios, number_of_items_per_prio, number_of_prios, t.elapsed().unwrap());
    let t = SystemTime::now();

    for _ii in (1..=number_of_items_per_prio).rev() {
        for e in 1..=number_of_prios {
            // println!("{}/{}: {:?}", ii, e, queue.peek());
            let i = (e % (256 - 3)) as u8;
            assert_eq!(queue.peek(), Some(vec![i, i + 1, i + 2]));
            assert_eq!(queue.pop(), Some(vec![i, i + 1, i + 2]));
            total_pops += 1;
        }
    }
    println!("Total pops: {:#3} in {:?}; that's {:?} per pop\n============================================\n",
             total_pops, t.elapsed().unwrap(), t.elapsed().unwrap().checked_div(total_pops).unwrap());
    assert!(queue.is_empty());
    assert_eq!(queue.pop(), None);
}

#[test]
fn it_works() {
    let mut queue = PriorityQueueImpl::new();
    assert!(queue.is_empty());

    queue.insert(vec![0], 5);
    assert!(!queue.is_empty());
    assert_eq!(queue.peek(), Some(vec![0]));

    queue.insert(vec![1], 10);
    queue.insert(vec![2], 3);
    queue.insert(vec![3], 4);
    queue.insert(vec![255], 6);

    // queue.print();
    assert_eq!(queue.pop(), Some(vec![1]));
    assert_eq!(queue.pop(), Some(vec![255]));
    assert_eq!(queue.pop(), Some(vec![0]));
    assert_eq!(queue.pop(), Some(vec![3]));
    assert_eq!(queue.pop(), Some(vec![2]));

    assert!(queue.is_empty());

    assert_eq!(queue.pop(), None);
}

#[test]
fn merge_priorities_test() {
    let mut queue = PriorityQueueImpl::new();

    queue.insert("Bryan".to_string().into_bytes(), 255);
    assert_eq!(queue.peek().unwrap(), "Bryan".to_string().into_bytes());


    queue.insert(vec![111], 258);
    assert_eq!(queue.peek(), Some(vec![111]));

    //Add to prio 258...
    queue.insert(vec![211, 212, 213], 258);
    assert_eq!(queue.peek(), Some(vec![111]));

    // Add to prio 255...
    queue.insert("Bette".to_string().into_bytes(), 255);

    //Prio 258 is still highest...
    assert_eq!(queue.pop(), Some(vec![111]));
    assert_eq!(queue.peek(), Some(vec![211, 212, 213]));
    //Remove the last from prio 258...
    assert_eq!(queue.pop(), Some(vec![211, 212, 213]));

    //prio 255 is now highest...
    assert_eq!(as_string(queue.peek()), r#"Bryan"#.to_string());

    // Add more items to prio 255...
    queue.insert("Acala".to_string().into_bytes(), 255);
    queue.insert("Laminar".to_string().into_bytes(), 255);

    //Read and Remove prio 255 items one by one...
    assert_eq!(as_string(queue.peek()), "Bryan".to_string());
    assert_eq!(as_string(queue.pop()), r#"Bryan"#.to_string());
    assert_eq!(as_string(queue.peek()), r#"Bette"#.to_string());
    assert_eq!(as_string(queue.pop()), r#"Bette"#.to_string());
    assert_eq!(as_string(queue.peek()), r#"Acala"#.to_string());
    assert_eq!(as_string(queue.pop()), r#"Acala"#.to_string());
    assert_eq!(as_string(queue.peek()), r#"Laminar"#.to_string());
    assert_eq!(as_string(queue.pop()), r#"Laminar"#.to_string());

    assert!(queue.is_empty());
}


#[test]
fn u64_to_vec_u8() {
    let mut v: Vec<u8> = vec!(255; 8);
    let mut x: Vec<u8> = u64::max_value().to_be_bytes().into();
    assert_eq!(x, v);

    v = vec!(255, 255, 255, 255, 255, 255, 255, 0);
    x = (u64::max_value() - 255).to_be_bytes().into();
    assert_eq!(x, v);

    //value = 1...
    v = vec!(0, 0, 0, 0, 0, 0, 0, 255);
    x = 255_u64.to_be_bytes().into();
    assert_eq!(x, v);

    //value = 258 = 256 + 2...
    v = vec!(0, 0, 0, 0, 0, 0, 1, 2);
    x = 258_u64.to_be_bytes().into();
    assert_eq!(x, v);

    //value = 72623859790382856...
    v = vec!(1, 2, 3, 4, 5, 6, 7, 8);
    x = (((1_u64 << 56) + (2 << 48) + (3 << 40) + (4 << 32) + (5 << 24) + (6 << 16) + (7 << 8) + 8) as u64).to_be_bytes().into();
    assert_eq!(x, v);
}

#[test]
fn vec_compare_tests() {
    let mut x: Vec<u8> = 1234_u64.to_be_bytes().into();
    let mut y: Vec<u8> = 1234_u64.to_be_bytes().into();
    assert_eq!(x, y);

    x = 255_u64.to_be_bytes().into();
    y = 258_u64.to_be_bytes().into();
    assert!(x < y);

    x = 22_u64.to_be_bytes().into();
    y = 20_u64.to_be_bytes().into();
    assert!(x > y);

    x = u64::max_value().to_be_bytes().into();
    y = (u64::max_value() - 1).to_be_bytes().into();
    assert!(x > y);
}